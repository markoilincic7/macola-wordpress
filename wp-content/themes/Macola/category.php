<?php 
/*
Template Name: Category Template
*/

?> 


<?php get_header(); ?>

<?php 


session_start();

if(!isset($_SESSION['category'])){
	$_SESSION['category'] = "zastitne_futrole";
}

if(!isset($_SESSION['page'])){
	$_SESSION['page'] = 0;
}

if(!isset($_SESSION['tag'])){
	$_SESSION['tag'] = "iPhone";
}




if (isset($_GET['tag'])){
  		$_SESSION['tag'] = $_GET['tag']; 
} 
else {
  
}
if(isset($_GET['category'])){
		 $_SESSION['category'] = $_GET['category']; 
}
else{

}
$args = array('posts_per_page'   => -1,'tag' =>  $_SESSION['tag'], 'category_name' =>  $_SESSION['category']);
$myposts = get_posts($args);

$a = 0;
$brojac = 0;
while( $myposts[$a] !=null) { $brojac++ ; $a++;}

if($brojac%15 == 0){
	$page_num = floor($brojac/15);
}
else{
	$page_num = floor($brojac/15) + 1;
}


$curr_page = $_GET['curr_page'];
$z =0 + $_GET['curr_page']*15;
$broj = 0; 
while( $myposts[$z] != null) { $broj++; $z++; }

$row_num = 0; 

if ($broj%15 == 0 && $broj!=0){
	
	$row_num = 5; 
} else {
	 

	if ($broj%3 == 0){
	$row_num = $broj/3; 
	} 
	else {
	$row_num = floor($broj/3) + 1; 
	}

	if($row_num>5){$row_num = 5;} 

}
?>



<hr>

			<div class="post-head">
				<div class="post-heading">
					<h1><?php $obj = get_category_by_slug($_SESSION['category']);
												echo $obj->cat_name;  ?>
					</h1>
				</div>
				<div class="category-address">
					<h4><a href="<?php echo home_url(); ?>">Naslovna Strana</a> > <a href="?category=<?php echo $_SESSION['category'];?>&tag=<?php echo $_SESSION['tag'];?>">Katalog Artikala</a> > <a href=""><?php echo $_SESSION['tag']?></a> > <a href=""><?php $obj = get_category_by_slug($_SESSION['category']);
												echo $obj->cat_name; ?></a></h4>
				</div>
			</div>
			<hr>
			<div class="category-content">
				<div class="category-navigation">
					<div class="category-nav">
						<ul>
							<li <?php if( $_SESSION["tag"] == "Desktop") {?> class ="active" <?php } ?>><a href="?tag=Desktop">Desktop</a></li>
							<li <?php if( $_SESSION["tag"] == "Laptop") {?> class ="active" <?php } ?>><a href="?tag=Laptop">Laptop</a></li>
							<li <?php if( $_SESSION["tag"] == "iPhone") {?> class ="active" <?php } ?> ><a href="?tag=iPhone">iPhone</a></li>
							<li <?php if( $_SESSION["tag"] == "iPad mini") {?> class ="active" <?php } ?>><a href="?tag=iPad mini">iPad mini</a></li>
							<li <?php if( $_SESSION["tag"] == "iPad") {?> class ="active" <?php } ?>><a href="?tag=iPad">iPad</a></li>
							<li <?php if( $_SESSION["tag"] == "iPod touch") {?> class ="active" <?php } ?>><a href="?tag=iPod touch">iPod touch</a></li>
							<li <?php if( $_SESSION["tag"] == "Ostalo") {?> class ="active" <?php } ?>><a href="?tag=Ostalo">Ostalo</a></li>
						</ul>
					</div>
					<div class="category-nav drop-nav">
						<ul>
							<li class="dropdown">
									<a class="dropdown-toggle" data-toggle="dropdown" href="#">
											<?php $obj = get_category_by_slug($_SESSION['category']);
												echo $obj->cat_name; 
											?>
									<div class="caret-border">
										<b class="caret"></b>
									</div>
									</a>
									<ul class="dropdown-menu">
										<li <?php if($_SESSION["category"] == "baterije") {?> style ="display:none" <?php } ?>><a href="?category=baterije&tag=<?php echo $_SESSION['tag'];?>">Baterije</a></li>
										<li <?php if($_SESSION["category"] == "punjaci") {?> style ="display:none" <?php } ?>><a href="?category=punjaci&tag=<?php echo $_SESSION['tag'];?>">Punjaci</a></li>
										<li <?php if($_SESSION["category"] == "zvucnici") {?> style ="display:none" <?php } ?>><a href="?category=zvucnici&tag=<?php echo $_SESSION['tag'];?>">Zvucnici</a></li>
										<li <?php if($_SESSION["category"] == "zastitne_futrole") {?> style ="display:none" <?php } ?>><a href="?category=zastitne_futrole&tag=<?php echo $_SESSION['tag'];?>">Zastitne futrole</a></li>
										<li <?php if($_SESSION["category"] == "Kablovi") {?> style ="display:none" <?php } ?>><a href="?category=Kablovi&tag=<?php echo $_SESSION['tag'];?>">Kablovi</a></li>
										<li <?php if($_SESSION["category"] == "Zvucnici") {?> style ="display:none" <?php } ?>><a href="?category=Zvucnici&tag=<?php echo $_SESSION['tag'];?>">Zvucnici</a></li>
										<li <?php if($_SESSION["category"] == "Periferije") {?> style ="display:none" <?php } ?>><a href="?category=baterije&tag=<?php echo $_SESSION['tag'];?>">Baterije</a></li>
										<li class="divider"></li>
										<li <?php if($_SESSION["category"] == "ostalo") {?> style ="display:none" <?php } ?>><a href="?category=ostalo&tag=<?php echo $_SESSION['tag'];?>">Ostalo</a></li>
									</ul>
							</li>
						</ul>
					</div>
				<?php if($page_num!=0) { ?>
					<div class="category-pagination pagination-right">
						<ul>
							<?php if($curr_page!=0) {?>
							<li><a href="?category= <?php echo $_SESSION['category'];?>&tag=<?php echo $_SESSION['tag']?>&curr_page=<?php echo $_GET['curr_page']-1;?>">&laquo;</a></li>
							<?php }?>
							<?php for($j=1; $j<=$page_num; $j++) {?>
							<li><a href="?category= <?php echo $_SESSION['category'];?>&tag=<?php echo $_SESSION['tag']?>&curr_page=<?php echo $j-1;?>"><?php echo $j;?></a></li>
							<?php } ?>
							<?php if($curr_page!=$page_num-1) {?>
							<li><a href="?category= <?php echo $_SESSION['category'];?>&tag=<?php echo $_SESSION['tag']?>&curr_page=<?php echo $_GET['curr_page']+1;?>">&raquo;</a></li>
							<?php }?>
						</ul>
					</div>
					<?php } ?>
				</div>

		
				<div class="category-items">
					<?php $count=0; for ($j=0; $j < $row_num ; $j++) { $count++; ?>
					<div class="items-row">
						<hr>
						<div class="row-fluid items-content item-images">
							<div class="span4">
								
								<a id="product" href="<?php echo "#" . $count;?>"><?php echo get_the_post_thumbnail($myposts[$j*3 + $_GET['curr_page']*15]->ID); ?></a>
								<div style="display:none;">
									<div id="<?php echo $count;?>" >
										<div class="row-fluid">
											<div class="product-images">
												<img src="<?php the_field('fancy_img1', $myposts[$j*3 + $_GET['curr_page']*15]->ID);?>">
												<img src="<?php the_field('fancy_img2', $myposts[$j*3 + $_GET['curr_page']*15]->ID);?>">
												<img src="<?php the_field('fancy_img3', $myposts[$j*3 + $_GET['curr_page']*15]->ID);?>">
											</div>
											<div class="product-text">
												<div class="product-name">
													<h2><?php echo $myposts[$j*3 + $_GET['curr_page']*15]->post_title; ?></h2>
												</div>
												<div class="product-compatibility">
													<h4>Kompatibilno sa:</h4>
													<?php
														$posttags = get_the_tags($myposts[$j*3 + $_GET['curr_page']*15]->ID);
														if ($posttags) { ?> 
														<div class="product-tags">
														<?php	foreach($posttags as $tag) { ?>

														<span><?php echo $tag->name;?></span>

														<?php } } ?>
														
													</div>
												</div>
												<div class="product-description">
													<?php echo $myposts[$j*3 + $_GET['curr_page']*15]->post_content; ?>
												</div>
												<div class="product-price">
													<?php the_field('cena', $myposts[$j*3 + $_GET['curr_page']*15]->ID); ?>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="span4">
								<?php $count++; ?>
								<a id="product" href="<?php echo "#" . $count;?>"><?php echo get_the_post_thumbnail($myposts[$j*3+1 + $_GET['curr_page']*15]->ID); ?></a>
								<div style="display:none;">
									<div id="<?php echo $count; ?>">
										<div class="row-fluid">
											<div class="product-images">
												<img src="<?php the_field('fancy_img1', $myposts[$j*3+1 + $_GET['curr_page']*15]->ID);?>">
												<img src="<?php the_field('fancy_img2', $myposts[$j*3+1 + $_GET['curr_page']*15]->ID);?>">
												<img src="<?php the_field('fancy_img3', $myposts[$j*3+1 + $_GET['curr_page']*15]->ID);?>">
											</div>
											<div class="product-text">
												<div class="product-name">
													<h2><?php echo $myposts[$j*3+1 + $_GET['curr_page']*15]->post_title; ?></h2>
												</div>
												<div class="product-compatibility">
													<h4>Kompatibilno sa:</h4>
													<?php
														$posttags = get_the_tags($myposts[$j*3+1 + $_GET['curr_page']*15]->ID);
														if ($posttags) { ?> 
														<div class="product-tags">
														<?php	foreach($posttags as $tag) { ?>

														<span><?php echo $tag->name;?></span>

														<?php } } ?>
														
													</div>
												</div>
												<div class="product-description">
													<?php $myposts[$j*3+1 + $_GET['curr_page']*15]->post_content; ?>
												</div>
												<div class="product-price">
													<?php the_field('cena', $myposts[$j*3+1 + $_GET['curr_page']*15]->ID); ?>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="span4">
								
									<?php $count++; ?>
								<a id="product" href="<?php echo "#" . $count;?>"><?php echo get_the_post_thumbnail($myposts[$j*3+2 + $_GET['curr_page']*15]->ID); ?></a>
								<div style="display:none;">
									<div id="<?php echo $count; ?>">
										<div class="row-fluid">
											<div class="product-images">
												<img src="<?php the_field('fancy_img1', $myposts[$j*3+2 + $_GET['curr_page']*15]->ID);?>">
												<img src="<?php the_field('fancy_img2', $myposts[$j*3+2 + $_GET['curr_page']*15]->ID);?>">
												<img src="<?php the_field('fancy_img3', $myposts[$j*3+2 + $_GET['curr_page']*15]->ID);?>">
											</div>
											<div class="product-text">
												<div class="product-name">
													<h2><?php echo $myposts[$j*3+2 + $_GET['curr_page']*15]->post_title; ?></h2>
												</div>
												<div class="product-compatibility">
													<h4>Kompatibilno sa:</h4>
													<?php
														$posttags = get_the_tags($myposts[$j*3+2 + $_GET['curr_page']*15]->ID);
														if ($posttags) { ?> 
														<div class="product-tags">
														<?php	foreach($posttags as $tag) { ?>

														<span><?php echo $tag->name;?></span>

														<?php } } ?>
														
													</div>
												</div>
												<div class="product-description">
													<?php $myposts[$j*3+2 + $_GET['curr_page']*15]->post_content; ?>
												</div>
												<div class="product-price">
													<?php the_field('cena', $myposts[$j*3+2 + $_GET['curr_page']*15]->ID); ?>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<hr>
						<div class="row-fluid items-content item-names">
							<div class="span4">
								<h4><?php echo $myposts[$j*3 + $_GET['curr_page']*15]->post_title; ?></h4>
							</div>
							<div class="span4">
								<h4><?php echo $myposts[$j*3+1 + $_GET['curr_page']*15]->post_title; ?></h4>
							</div>
							<div class="span4">
								<h4><?php echo $myposts[$j*3+2 + $_GET['curr_page']*15]->post_title; ?></h4>
							</div>
						</div>
						<div class="row-fluid items-content">
							<div class="span4">
								<h5 class="item-description"><?php the_field('kratak_opis', $myposts[$j*3 + $_GET['curr_page']*15]->ID);?></h5>
							</div>
							<div class="span4">
								<h5 class="item-description"><?php the_field('kratak_opis', $myposts[$j*3+1 + $_GET['curr_page']*15]->ID);?></h5>
							</div>
							<div class="span4">
								<h5 class="item-description"><?php the_field('kratak_opis', $myposts[$j*3+2 + $_GET['curr_page']*15]->ID);?></h5>
							</div>
						</div>
						<div class="row-fluid items-content item-prices">
							<div class="span4">
								<h4><?php the_field('cena', $myposts[$j*3 + $_GET['curr_page']*15]->ID);?></h4>
							</div>
							<div class="span4">
								<h4><?php the_field('cena', $myposts[$j*3+1 + $_GET['curr_page']*15]->ID);?></h4>
							</div>
							<div class="span4">
								<h4><?php the_field('cena', $myposts[$j*3+2 + $_GET['curr_page']*15]->ID);?></h4>
							</div>
						</div>
					</div>	
					<?php } ?>		
	
					<hr class="category-endhr">	

				</div> 
				<div class="category-navigation">
					<div class="category-nav">
						<ul>
							<li <?php if( $_SESSION["tag"] == "Desktop") {?> class ="active" <?php } ?>><a href="?tag=Desktop">Desktop</a></li>
							<li <?php if( $_SESSION["tag"] == "Laptop") {?> class ="active" <?php } ?>><a href="?tag=Laptop">Laptop</a></li>
							<li <?php if( $_SESSION["tag"] == "iPhone") {?> class ="active" <?php } ?> ><a href="?tag=iPhone">iPhone</a></li>
							<li <?php if( $_SESSION["tag"] == "iPad mini") {?> class ="active" <?php } ?>><a href="?tag=iPad mini">iPad mini</a></li>
							<li <?php if( $_SESSION["tag"] == "iPad") {?> class ="active" <?php } ?>><a href="?tag=iPad">iPad</a></li>
							<li <?php if( $_SESSION["tag"] == "iPod touch") {?> class ="active" <?php } ?>><a href="?tag=iPod touch">iPod touch</a></li>
							<li <?php if( $_SESSION["tag"] == "Ostalo") {?> class ="active" <?php } ?>><a href="?tag=Ostalo">Ostalo</a></li>
						</ul>
					</div>
					<div class="category-nav drop-nav">
						<ul>
							<li class="dropdown">
									<a class="dropdown-toggle" data-toggle="dropdown" href="#">
											<?php $obj = get_category_by_slug($_SESSION['category']);
												echo $obj->cat_name; 
											?>
									<div class="caret-border">
										<b class="caret"></b>
									</div>
									</a>
									<ul class="dropdown-menu">
										<li <?php if($_SESSION["category"] == "baterije") {?> style ="display:none" <?php } ?>><a href="?category=baterije&tag=<?php echo $_SESSION['tag'];?>">Baterije</a></li>
										<li <?php if($_SESSION["category"] == "punjaci") {?> style ="display:none" <?php } ?>><a href="?category=punjaci&tag=<?php echo $_SESSION['tag'];?>">Punjaci</a></li>
										<li <?php if($_SESSION["category"] == "zvucnici") {?> style ="display:none" <?php } ?>><a href="?category=zvucnici&tag=<?php echo $_SESSION['tag'];?>">Zvucnici</a></li>
										<li <?php if($_SESSION["category"] == "zastitne_futrole") {?> style ="display:none" <?php } ?>><a href="?category=zastitne_futrole&tag=<?php echo $_SESSION['tag'];?>">Zastitne futrole</a></li>
										<li class="divider"></li>
										<li <?php if($_SESSION["category"] == "ostalo") {?> style ="display:none" <?php } ?>><a href="?category=ostalo&tag=<?php echo $_SESSION['tag'];?>">Ostalo</a></li>
									</ul>
							</li>
						</ul>
					</div>
					<?php if($page_num!=0) { ?>
					<div class="category-pagination pagination-right">
						<ul>
							<?php if($curr_page!=0) {?>
							<li><a href="?category= <?php echo $_SESSION['category'];?>&tag=<?php echo $_SESSION['tag']?>&curr_page=<?php echo $_GET['curr_page']-1;?>">&laquo;</a></li>
							<?php }?>
							<?php for($j=1; $j<=$page_num; $j++) {?>
							<li><a href="?category= <?php echo $_SESSION['category'];?>&tag=<?php echo $_SESSION['tag']?>&curr_page=<?php echo $j-1;?>"><?php echo $j;?></a></li>
							<?php } ?>
							<?php if($curr_page!=$page_num-1) {?>
							<li><a href="?category= <?php echo $_SESSION['category'];?>&tag=<?php echo $_SESSION['tag']?>&curr_page=<?php echo $_GET['curr_page']+1;?>">&raquo;</a></li>
							<?php }?>
						</ul>
					</div>
					<?php } ?>
				</div>
			</div>
		</div>
		<?php get_footer(); ?>