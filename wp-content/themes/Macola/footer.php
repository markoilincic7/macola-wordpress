		<div class="footer">
			<div class="widgets">
				<span class="widget-item">
					<h2 class="widget-name">Prodajna mesta:</h2>
					<div class="widget-content">
						Andrićev Venac 12<br>
						11000 Beograd, Srbija<br>
						011/33-49-298<br>
						<br>
						Bul. Mihaila Pupina 168b<br>
						11000 Beograd, Srbija<br>
						011/311-98-18
					</div>
					

					<div class="widget-content">
						<h2><?php eemail_show();?></h2>
					</div>
				</span>
				<span class="widget-item">
					<h2 class="widget-name">Aktuelne akcije:</h2>
					<div class="widget-content">
						<?php query_posts( array( 'cat' => 5, 'posts_per_page' => 3) ); ?>
 							<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

						<a href="<?php the_permalink();?>"><div class="post">
							<div class="post-image"> <?php the_post_thumbnail(); ?> </div>
				
								<div class="post-description">
									<div class="post-name"><h5><?php the_title(); ?></h5></div>
									<div class="post-text"><h5><?php echo get_the_content(); ?><h5></div>
								</div>
							
							</div> 
						</a>

						<?php endwhile;?>
        						<?php endif;?>		
					</div>
				</span>
				<span class="widget-item">
					<h2 class="widget-name">Poslednje vesti:</h2>
					<div class="widget-content">
						<?php query_posts( array( 'cat' => 6, 'posts_per_page' => 3) ); ?>
 							<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

 						<a href="<?php the_permalink();?>">		
							<div class="post">

								<div class="post-image"><?php the_post_thumbnail();?></div>
								<div class="post-description">
									<div class="post-name"><h5><?php the_title(); ?></h5></div>
									<div class="post-text"><h5><?php echo get_the_content(); ?><h5></div>
								</div>
							</div>
						</a>
						<?php endwhile;?>
        						<?php endif;?>		
					</div>
				</span>
			</div>
			<?php wp_nav_menu(array('menu' => 'Footer Menu')); ?> 
			<div class="made-on-mac">
				<img src="<?php echo site_url(); ?>/wp-content/themes/BlankTheme/img/made_on_mac.png" alt="Logo">
			</div>
		</div>
	</div>
	


	


	<script type="text/javascript" src="<?php echo site_url(); ?>/wp-content/themes/BlankTheme/js/jquery-1.10.2.min.js"></script>
	<script type="text/javascript" src="<?php echo site_url(); ?>/wp-content/themes/BlankTheme/js/bootstrap.js"></script>
	<script type="text/javascript" src="<?php echo site_url(); ?>/wp-content/themes/BlankTheme/js/jquery.fancybox.pack.js"></script>

	<script>
	  $(document).ready(function () {
	  	
	    $('.carousel').carousel({
	        interval: 3000
	    });

	    $('.carousel').carousel('cycle');

	    $('#0').click({
	    	.carousel(0)
	    });
	    $('#1').click({
	    	.carousel(1)
	    });
	    $('#2').click({
	    	.carousel(2)
	    });
	    $('#3').click({
	    	.carousel(3)
	    });
	  });

	  $(document).ready(function() {
	  	$(window).scroll({
	  		alert(1);
	  	});
	  });
	</script>

	<script type="text/javascript">
		$(function(){
	    var header = $('.header'),
	        pos = header.offset();
	        $(window).scroll(function(){
	            if($(this).scrollTop() > pos.top+header.height() && header.hasClass('header')){
	                header.fadeOut('fast', function(){
	                    $(this).removeClass('header').addClass('header-small').fadeIn('fast');
	                });
	            } else if($(this).scrollTop() <= pos.top && header.hasClass('header-small')){
	                header.fadeOut('fast', function(){
	                    $(this).removeClass('header-small').addClass('header').fadeIn('fast');
	                });
	            }
	        });
		});


		/* FANCYBOX */
	$(document).ready(function() {
			
			$("a#product").fancybox({
				'titleShow'     : false,
				'openEffect'	: 'elastic',
				'closeEffect'	: 'elastic',
				'openSpeed'		: 'fast',
				'closeSpeed'	: 'fast',
				'openMethod'	: 'zoomIn',
				'closeMethod'	: 'zoomOut',
				'padding'		: 0,
				'margin'		: [40,0,0,0],

				helpers : {
					overlay : {
						locked : false
					}
				}
			});

	});
	</script>

<?php wp_footer(); ?>	
</body>
</html>