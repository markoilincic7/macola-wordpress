 <?php 
/*
Template Name: Specijanli artikal Template
*/

?> 

<?php get_header(); the_post(); ?>

<hr>
			<div class="minibar-nav nav">
				<?php wp_nav_menu(array('menu' => 'ipod_ipad')); ?> 
			</div>
			<div class="row-fluid hero">
				<div class="span6 image">
					<img src="<?php the_field('big_post_image'); ?>">
				</div>								
			</div>

			<div class="beige grid4">
				<div class="row-fluid">
					<div class="span3">
						<div class="grid4-image-container">
							<img src="<?php the_field('img1'); ?>" > 
						</div>
					</div>
					<div class="span3">
						<div class="grid4-image-container">
							<img src="<?php the_field('img2'); ?>" > 
						</div>
					</div>
					<div class="span3">
						<div class="grid4-image-container">
							<img src="<?php the_field('img3'); ?>" > 
						</div>
					</div>
					<div class="span3">
						<div class="grid4-image-container">
							<img src="<?php the_field('img4'); ?>" > 
						</div>
					</div>
				</div>
				<div class="row-fluid">
					<div class="span3 grid4-heading">
						<h2><?php the_field('title1'); ?></h2>
					</div>
					<div class="span3 grid4-heading">
						<h2><?php the_field('title2'); ?></h2>
					</div>
					<div class="span3 grid4-heading">
						<h2><?php the_field('title3'); ?></h2>
					</div>
					<div class="span3 grid4-heading">
						<h2><?php the_field('title4'); ?></h2>
					</div>
				</div>
				<div class="row-fluid">
					<div class="span3 grid4-description">
						<div class="grid4-description">
							<h4><?php the_field('content1'); ?></h4>
						</div>
					</div>
					<div class="span3">
						<div class="grid4-description">
							<h4><?php the_field('content2'); ?></h4>
						</div>
					</div>
					<div class="span3">
						<div class="grid4-description">
							<h4><?php the_field('content3'); ?></h4>
						</div>
					</div>
					<div class="span3">
						<div class="grid4-description">
							<h4><?php the_field('content4'); ?></h4>
						</div>
					</div>
				</div>
			</div>
			<div class="grid2">
				<div class="row-fluid">
					<div class="span6 grid2-text">
						<div class="row-fluid grid2-textrow">
							<div class="grid2-heading">
								<h1><?php the_field('title5'); ?></h1>
							</div>
							<div class="grid2-description">
								<h3><?php the_field('content5'); ?></h3>
							</div>							
						</div>
						<div class="row-fluid">
							<div class="grid2-heading">
								<h1><?php the_field('title52'); ?></h1>
							</div>
							<div class="grid2-description">
								<h3><?php the_field('content52'); ?></h3>
							</div>
						</div>
					</div>
					<div class="span6 grid2-image-container">
						<img class="grid2-image" src="<?php the_field('img5'); ?>">
					</div>
				</div>
			</div>
			<hr>
			<div class="grid2">
				<div class="row-fluid">
					<div class="span6 grid2-image-container">
						<img class="grid2-image" src="<?php the_field('img6'); ?>">
					</div>
					<div class="span6 grid2-text-right">
						<div class="row-fluid grid2-textrow ">
							<div class="grid2-heading">
								<h1><?php the_field('title6'); ?></h1>
							</div>
							<div class="grid2-description">
								<h3><?php the_field('content6'); ?></h3>
							</div>							
						</div>
						<div class="row-fluid">
							<div class="grid2-heading">
								<h1><?php the_field('title62'); ?></h1>
							</div>
							<div class="grid2-description">
								<h3><?php the_field('content62'); ?></h3>
							</div>
						</div>
					</div>					
				</div>
			</div>
			<hr>
			<div class="grid2">
				<div class="row-fluid">
					<div class="span6 grid2-text">
						<div class="row-fluid">
							<div class="grid2-heading">
								<h1><?php the_field('title7'); ?></h1>
							</div>
							<div class="grid2-description">
								<h3><?php the_field('content7'); ?></h3>
							</div>							
						</div>
					</div>
					<div class="span6 grid2-image-container">
						<img class="grid2-image" src="<?php the_field('img7'); ?>">
					</div>
				</div>
			</div>
			<div class="beige prices">
				<?php echo the_field('cenovnik_spec_aritkli')?>
			</div>
			<div class="square">
				<div class="row-fluid">
					<div class="span6 square-span">
						<div class="square-imagecontainer">
							<img class="square-image" src="<?php echo site_url(); ?>/wp-content/themes/BlankTheme/img/specijalni_artikl/square1.png">
						</div>
						<div class="square-block">
							<div class="square-heading">
								<h2><?php the_field('popusti_title')?></h2>
							</div>
							<div class="square-text">
								<?php the_field('popusti_body')?>
								<p> </p>
								<p>
									<a href="">Zašto iPad za fakultet? ></a>
								</p>
							</div>
						</div>
					</div>
					<div class="span6 square-span">
						<div class="square-imagecontainer">
							<img class="square-image" src="<?php echo site_url(); ?>/wp-content/themes/BlankTheme/img/specijalni_artikl/square2.png">
						</div>
						<div class="square-block">
							<div class="square-heading">
								<h2>Specijalizovani dodaci</h2>
							</div>
							<div class="square-text">
								<p>
									Ukoliko vas zanimaju specijalizovani dodaci za vaš iOS uređaj,
									posetite jednu od sledećih kategorija:
								</p>
								<p>
									<a href="<?php echo site_url()?>/dodatci/?tag=<?php echo single_post_title(); ?>">Dodaci ></a>
								</p>
								<p>
									<a href="<?php echo site_url()?>/dodatci/?tag=<?php echo single_post_title(); ?>&category=punjaci">Punjaci ></a>
								</p>
								<p>
									<a href="<?php echo site_url()?>/dodatci/?tag=<?php echo single_post_title(); ?>&category=Kablovi">Kablovi ></a>
								</p>
								<p>
									<a href="<?php echo site_url()?>/dodatci/?tag=<?php echo single_post_title(); ?>&category=Zvucnici">Zvučnici ></a>
								</p>
								<p>
									<a href="<?php echo site_url()?>/dodatci/?tag=<?php echo single_post_title(); ?>&category=Periferije">Periferije ></a>
								</p>
							</div>
						</div>
					</div>
				</div>
				<div class="row-fluid">
					<div class="span6 square-span">
						<div class="square-imagecontainer">
							<img class="square-image" src="<?php echo site_url(); ?>/wp-content/themes/BlankTheme/img/specijalni_artikl/square3.png">
						</div>
						<div class="square-block">
							<div class="square-heading">
								<h2>Šta je u kutiji</h2>
							</div>
							<div class="square-text">
								<?php the_field('kutija'); ?>
							</div>
						</div>
					</div>
					<div class="span6 square-span">
						<div class="square-imagecontainer">
							<img class="square-image" src="<?php echo site_url(); ?>/wp-content/themes/BlankTheme/img/specijalni_artikl/square4.png">
						</div>
						<div class="square-block">							
							<div class="square-heading">
								<h2>Tehničke specifikacije:</h2>
							</div>
							<div class="square-text">
								<?php the_field('specifikacije'); ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>



<?php get_footer(); ?>