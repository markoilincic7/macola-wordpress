<?php 
/*
Template Name: Izbornik_Mac Template
*/

?> 

<?php get_header(); ?>

<hr>
			<div class="minibar-nav nav">
				<?php wp_nav_menu(array('menu' => 'mac_menu')); ?> 
			</div>
			<div class="row-fluid hero">
				<div class="span6 image">
					<img src="<?php echo site_url(); ?>/wp-content/themes/BlankTheme/img/izbornik/hero_mac.png">
				</div>								
			</div>


			
			<?php $args = array( 'posts_per_page' => 6, 'category__in' => array(9) );
						$myposts = get_posts( $args ); ?>

			<div class="padding-content grid3">
				<div class="grid3-row">
					<div class="row-fluid">
						<div class="span4">
							<div class="grid3-image"><?php echo get_the_post_thumbnail($myposts[5]->ID); ?></div>
						</div>
						<div class="span4">
							<div class="grid3-image"><?php echo get_the_post_thumbnail($myposts[4]->ID); ?></div>
						</div>
						<div class="span4">
							<div class="grid3-image"><?php echo get_the_post_thumbnail($myposts[3]->ID); ?></div>
						</div>
					</div>
					<div class="row-fluid">
						<div class="span4">
							<h2><div class="grid3-heading"><?php echo $myposts[5]->post_title; ?></div></h2>
						</div>
						<div class="span4">
							<h2><div class="grid3-heading"><?php echo $myposts[4]->post_title; ?></div></h2>
							
						</div>
						<div class="span4">
							<h2><div class="grid3-heading"><?php echo $myposts[3]->post_title; ?></div></h2>
						</div>
					</div>
					<div class="row-fluid">
						<div class="span4">
							<div class="grid3-description">
								<h4><?php echo $myposts[5]->post_content ; ?></h4>
							</div>
						</div>
						<div class="span4">
							<div class="grid3-description">
								<h4><?php echo $myposts[4]->post_content ; ?></h4>
							</div>
						</div>
						<div class="span4">
							<div class="grid3-description">
								<h4><?php echo $myposts[3]->post_content ; ?></h4>
							</div>
						</div>
					</div>
					<div class="row-fluid">
						<div class="span4">
							<a href="<?php echo get_permalink( $myposts[5]->ID);?>"><button type="button" class="grid3-more"><h2>Saznaj Više ></h2></button></a>
						</div>
						<div class="span4">
							<a href="<?php echo get_permalink( $myposts[4]->ID);?>"><button type="button" class="grid3-more"><h2>Saznaj Više ></h2></button></a>
						</div>
						<div class="span4">
							<a href="<?php echo get_permalink( $myposts[3]->ID);?>"><button type="button" class="grid3-more"><h2>Saznaj Više ></h2></button></a>
						</div>
					</div>
				</div>	
				<div class="grid3-row">
					<div class="row-fluid">
						<div class="span4">
							<div class="grid3-image"><?php echo get_the_post_thumbnail($myposts[2]->ID); ?></div>
						</div>
						<div class="span4">
							<div class="grid3-image"><?php echo get_the_post_thumbnail($myposts[1]->ID); ?></div>
						</div>
						<div class="span4">
							<div class="grid3-image"><?php echo get_the_post_thumbnail($myposts[0]->ID); ?></div>
						</div>
					</div>
					<div class="row-fluid">
						<div class="span4">
							<h2><div class="grid3-heading"><?php echo $myposts[2]->post_title; ?></div></h2>
						</div>
						<div class="span4">
							<h2><div class="grid3-heading"><?php echo $myposts[1]->post_title; ?></div></h2>
						</div>
						<div class="span4">
							<h2><div class="grid3-heading"><?php echo $myposts[0]->post_title; ?></div></h2>
						</div>
					</div>
					<div class="row-fluid">
						<div class="span4">
							<div class="grid3-description">
								<h4><?php echo $myposts[2]->post_content ; ?></h4>
							</div>
						</div>
						<div class="span4">
							<div class="grid3-description">
								<h4><?php echo $myposts[1]->post_content ; ?></h4>
							</div>
						</div>
						<div class="span4">
							<div class="grid3-description">
								<h4><?php echo $myposts[0]->post_content ; ?></h4>
							</div>
						</div>
					</div>
					<div class="row-fluid">
						<div class="span4">
							<a href="<?php echo get_permalink( $myposts[2]->ID);?>"><button type="button" class="grid3-more"><h2>Saznaj Više ></h2></button></a>
						</div>
						<div class="span4">
							<a href="<?php echo get_permalink( $myposts[1]->ID);?>"><button type="button" class="grid3-more"><h2>Saznaj Više ></h2></button></a>
						</div>
						<div class="span4">
							<a href="<?php echo get_permalink( $myposts[0]->ID);?>"><button type="button" class="grid3-more"><h2>Saznaj Više ></h2></button></a>
						</div>
					</div>
				</div>
			</div>
		</div>



 

<?php get_footer(); ?>