<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>
	<meta charset="<?php bloginfo('charset'); ?>" />
	
	<?php if (is_search()) { ?>
	   <meta name="robots" content="noindex, nofollow" /> 
	<?php } ?>

	<title>
		   <?php
		      if (function_exists('is_tag') && is_tag()) {
		         single_tag_title("Tag Archive for &quot;"); echo '&quot; - '; }
		      elseif (is_archive()) {
		         wp_title(''); echo ' Archive - '; }
		      elseif (is_search()) {
		         echo 'Search for &quot;'.wp_specialchars($s).'&quot; - '; }
		      elseif (!(is_404()) && (is_single()) || (is_page())) {
		         wp_title(''); echo ' - '; }
		      elseif (is_404()) {
		         echo 'Not Found - '; }
		      if (is_home()) {
		         bloginfo('name'); echo ' - '; bloginfo('description'); }
		      else {
		          bloginfo('name'); }
		      if ($paged>1) {
		         echo ' - page '. $paged; }
		   ?>
	</title>
	
	<link rel="shortcut icon" href="/favicon.ico">
	
	<link rel="stylesheet" href="<?php echo site_url(); ?>/wp-content/themes/BlankTheme/css/bootstrap.css">
	<link rel="stylesheet" href="<?php echo site_url(); ?>/wp-content/themes/BlankTheme/css/bootstrap.min.css">

	<link rel="stylesheet" href="<?php echo site_url(); ?>/wp-content/themes/BlankTheme/style.css">
	<link rel="stylesheet" type="text/css" href="<?php echo site_url(); ?>/wp-content/themes/BlankTheme/css/jquery.fancybox.css" media="screen" />


	<!-- IE -->
	
	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">

	<?php if ( is_singular() ) wp_enqueue_script('comment-reply'); ?>

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
	
	<div class="header">
		<div class="container">
			<div class="row">
				<div class="logo">
					<a href="<?php echo home_url(); ?>"><img src="<?php echo site_url(); ?>/wp-content/themes/BlankTheme/img/logo.png" alt="Logo"></a>
				</div>
			</div>
			<div class="row">
				<div class="header-nav nav">
					<?php wp_nav_menu(array('menu' => 'Main Nav Menu')); ?> 
				</div>
			</div>
		</div>		
	</div>
	<div class="content">
		<div class="container">
			<div class="minibar">
				<img src="<?php echo site_url(); ?>/wp-content/themes/BlankTheme/img/home/authorization.png">
				<form class="navbar-search pull-right" >
					<?php get_search_form(); ?>

				</form>
			</div>