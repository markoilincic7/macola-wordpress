<?php 
/*
Template Name: Home Template
*/

?> 


<?php get_header(); ?>


			<div id="slider" class="carousel slide">
				<div class="carousel-inner">
					<div class="item active"><img src="<?php echo site_url(); ?>/wp-content/themes/BlankTheme/img/home/slider/osx.png"></div>
					<div class="item"><img src="<?php echo site_url(); ?>/wp-content/themes/BlankTheme/img/home/slider/fusion_drive.png"></div>
					<div class="item"><img src="<?php echo site_url(); ?>/wp-content/themes/BlankTheme/img/home/slider/ipad.png"></div>
					<div class="item"><img src="<?php echo site_url(); ?>/wp-content/themes/BlankTheme/img/home/slider/macbook_air.png"></div>
				</div>
				<ol class="carousel-indicators">
					<li id="0" data-target="#slider" data-slide-to="0" class="active"></li>
					<li id="1" data-target="#slider" data-slide-to="1"></li>
					<li id="2" data-target="#slider" data-slide-to="2"></li>
					<li id="3" data-target="#slider" data-slide-to="3"></li>
				</ol>
			</div>
			<div class="news">

				<?php query_posts( array( 'cat' => 5, 'posts_per_page' => 3) ); $i=1; ?>
 							<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

 							<?php if(i!=3) { $i++; ?>
				<span class="item">
					<div class="postname">
						<h2 class="postheading"><?php the_title();?></h2>
						<div class="headingafter">
							<p> <?php echo get_the_content();?></p>
						</div>
					</div>
					<div class="postimage">
						<?php the_post_thumbnail(); ?>
					</div>
				</span>
							<?php } else { ?>

			
				<span class="item lastitem">
					<div class="postname">
						<h2 class="postheading"><?php the_title();?></h2>
						<div class="headingafter">
							<p><?php echo get_the_content();?></p>
						</div>
					</div>
					<div class="postimage">
						<?php the_post_thumbnail(); ?>
					</div>
				</span>
							<?php } ?>
							   <?php endwhile;?>
        							<?php endif;?>


			</div>
			<div class="social">
				<ul>
					<li><a href=""><h1>Prijatelji Macole</h1></a></li>
					<li><a href=""><h1>Aktuelno u Macoli</h1></a></li>
					<li><a href=""><h1>Socialne mreže</h1></a></li>
				</ul>
			</div>
			<div class="padding-content">
				<div class="row-fluid singleline-text">
					Naš cilj je da inspirišemo i zabavimo naše mušterije pri svakoj njihovoj poseti, tako da naša radnja postane pravo mesto da kupite, da naučite, da potražite pomoć i da sa osmehom na licu dođete ponovo!
				</div>
				<div class="row-fluid split2">
					<div class="span6 half half-left">
						Informisani smo o poslednjim Apple tehnologijama, a Vama su na usluzi
						naši Apple sertifikovani stručnjaci za servis i podršku.
						<br><br>
						Naš servis je savremen, dobro organizovan
						i kompletno opremljen po Apple standardima.
						<br><br>
						U našem Crossover ovlašćenom Apple trening centru možete pronaći
						kurseve u kojima će vas naši iskusni treneri ispratiti od osnova korišćenja
						do naprednog rada u profesionalnim programima.
						<br><br>
						Nakon ovoga, možete dokazati svoje znanje, položiti testove i dobiti
						internacionalno priznanje preko Apple sertifikata.
					</div>
					<div class="span6 half no-margins">
						<img src="<?php echo site_url(); ?>/wp-content/themes/BlankTheme/img/home/mac_shop.jpg">
					</div>
				</div>
				<div class="row-fluid split2">
					<div class="span6 half">
						<a href="index.php?page_id=17"><img src="<?php echo site_url(); ?>/wp-content/themes/BlankTheme/img/home/map.jpg"></a>
					</div>
					<div class="span6 half no-margins half-right">
						U našim izlošbenim prostorima će vas sačekati kompletan asortiman
						Apple opreme, svi modeli Macintosh računara: iMac, Mac Pro, MacBook Pro
						i MacBook Air. Najbolji tablet računari na svetu Apple iPad i iPad mini
						i najpoznatiji muzički plejer na svetu, iPod.
						Kod nas možete poručiti i posebno sastavljane BTO konfiguracije.
						<br><br>
						Mi stremimo da budemo drugačiji i da vam uvek ponudimo najzanimljiviju
						i najneobičniju dodatnu opremu, futrole torbe i gedžete, 
						po najboljim cenama.
						<br><br><br>
						Pronađite nas na dve loakcije u Beogradu:
						<br><br>
						- U samom centru grada kod skupštine
						<br><br>
						- Kao i na Novom Beogradu u YU Biznis centru
					</div>
				</div>
			</div>
		</div>
  
  <?php get_footer(); ?>