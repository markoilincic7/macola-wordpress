 <?php 
/*
Template Name: Singlepost Template
*/

?> 

<?php get_header(); the_post(); ?>

<hr>
<div class="post-head">
				<div class="post-heading">
					<h1> <?php the_title(); ?> </h1>
				</div>
				<div class="post-date">
					<h4><?php echo the_date(); ?> </h4>
				</div>
			</div>
			<div class="post-content">
				<div class="post-featured">
					<img class="post-featured-image" <?php the_post_thumbnail(); ?>
					<h5 class="figcaption"><?php echo get_post(get_post_thumbnail_id())->post_excerpt; ?></h5>
				</div>
				<div class="post-content-text">
					<h4>
						<?php the_content() ?>
					</h4>
					
					<img src="<?php the_field('slika_u_postu'); ?>">
				</div>
				<div class="post-tags">

					<?php
							$posttags = get_the_tags();
								if ($posttags) { ?> 
									<h3 class="post-tags-thead">Tagovi:</h3>
  									<?php	foreach($posttags as $tag) { ?>
							    	 <span class="post-tag"><h5><?php echo $tag->name;?></h5></span>
							  		<?php } } ?>
							
				

					<h3 class="post-readmore"><a href="">- Pročitajte više ></a></h3>
				</div>
			</div>
		</div>
		

 <?php get_footer(); ?>