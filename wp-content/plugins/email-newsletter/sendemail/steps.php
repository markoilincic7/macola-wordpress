<?php if(preg_match('#' . basename(__FILE__) . '#', $_SERVER['PHP_SELF'])) { die('You are not allowed to call this page directly.'); } ?>
<h3>Steps to Send Email</h3>
<ol>
  <li>Select email address from the list.</li>
  <li>Select available email subject.</li>
  <li>Click send email button.</li>
</ol>
